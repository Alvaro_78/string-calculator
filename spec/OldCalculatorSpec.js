
describe('String Calculator',()=>{

  let calculate = null

  beforeEach(()=>{
    calculate = new Calculator()
  })

  it('Is a  string argument is empty can returns  0 ',()=>{
    const string = ''

    const result = calculate.emptyString(string)

    expect(result).toEqual(0)
  })

  it('Is a string is 1 returns 1',()=>{
    const string = '1'

    const result = calculate.emptyString(string)

    expect(result).toEqual(1)
  })

  it('Is a  string are two numbers returns the sum of them ',()=>{
    const string = '1,2'

    const result = calculate.sum(string)

    expect(result).toEqual(3)
  })

  // it('Splits a string in an array', ()=>{
  //   const string = '1,2,3,4'
  //   const array = ["1","2","3","4"]
  //   const result = calculate.splitString(string)
  //
  //   expect(result).toEqual(array)
  // })
  it('Is a sum of numbers',()=>{
    const string = '5'

    const result = calculate.numSum()

    expect(result).toEqual(string)
  })

  it('Is a second sum of numbers',()=>{
    const string = '20'

    const result = calculate.secondSum()

    expect(result).toEqual(string)
  })

})
